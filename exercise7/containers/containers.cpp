/*
 * Programming Techniques for Scientific Simulations I
 * HS 2020
 * Exercise 7.2
 * 2020-10-29
 * Michal Sudwoj <msudwoj@student.ethz.ch>
 */

#include <iomanip>  // for std::setw, std::setprecision
#include <ios>      // for std::right
#include <iostream> // for std::cout
#include <vector>   // for std::vector
#include <list>     // for std::list
#include <set>      // for std::set
#include "timer/timer.hpp"

using test_type = unsigned long;

template <typename C>
void copy(const std::vector<test_type> &input, C &output)
{
    std::copy(input.begin(), input.end(), std::back_inserter(output));
}

template <>
void copy<std::set<test_type>>(const std::vector<test_type> &input, std::set<test_type> &output)
{
    for (auto it = input.begin(); it != input.end(); ++it)
    {
        output.insert(*it);
    }
}

template <typename C>
double measure_container(const size_t &num_ops,
                         const std::vector<test_type> &input)
{
    C container;
    copy(input, container);

    auto size = container.size();
    Timer t;
    t.start();
    for (std::size_t i = 0; i < num_ops; ++i)
    {
        container.insert(container.begin(), 0);
        container.erase(container.begin());
    }
    t.stop();
    if (size != container.size())
    {
        throw std::logic_error("this iz bugg");
    }

    return 1e9 * t.duration() / num_ops;
}

int main()
{
    const size_t num_ops = 4e3;

    std::cout
        << std::right << "# "
        << std::setw(4) << "N" << ' '
        << std::setw(13) << "Vector[ns/op]" << ' '
        << std::setw(13) << "List[ns/op]" << ' '
        << std::setw(13) << "Set[ns/op]" << '\n';
    for (unsigned i = 4; i < 14; ++i)
    {
        const size_t size = 1ul << i; // == std::pow(2, i)

        std::vector<test_type> input(size);

        for (size_t i = 0; i < input.size(); ++i)
        {
            input[i] = i + 1;
        }

        std::cout
            << std::right << std::fixed << std::setprecision(6) << "  "
            << std::setw(4) << size << ' '
            << std::setw(13)
            << measure_container<std::vector<test_type>>(num_ops, input) << ' '
            << std::setw(13)
            << measure_container<std::list<test_type>>(num_ops, input) << ' '
            << std::setw(13)
            << measure_container<std::set<test_type>>(num_ops, input) << '\n';
    }

    return 0;
}
