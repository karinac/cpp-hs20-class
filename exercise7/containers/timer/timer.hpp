#ifndef TIMER_HPP
#define TIMER_HPP
/*
 * Programming Techniques for Scientific Simulations I
 * HS 2020
 * Exercise 7.2
 * 2020-10-29
 * Michal Sudwoj <msudwoj@student.ethz.ch>
 */

#include <chrono>

class Timer
{
public:
  Timer();
  void start();
  void stop();
  double duration() const;

private:
  using time_point_t = std::chrono::high_resolution_clock::time_point;
  time_point_t tstart_;
  time_point_t tend_;
};

#endif // !defined TIMER_HPP
