#ifndef MY_ITERATOR_HPP
#define MY_ITERATOR_HPP
/*
 * Programming Techniques for Scientific Simulations I
 * HS 2020
 * Exercise 7.1
 * 2020-10-29
 * Michal Sudwoj <msudwoj@student.ethz.ch>
 */

#include <cassert>
#include <iostream> // for debugging
#include <iterator> // for iterator category tags
#include <stdexcept>
/*
Forward & bidirectional iterators requirements:

Iterator:
- CopyConstructible
- CopyAssignable
- Destructible
- Supports: *a (Dereferenceable)
- Supports: ++a (Preincrementable)

Input Iterator:
- All requirements of an iterator.
- Supports: == (EqualityComparable)
- Supports: !=
- Supports: ->
- Supports: a++ (Postincrementable)

Forward Iterator:
- All requirements of an input iterator
- DefaultConstructible
- Supports expression: *a++

Bidirectional Iterator:
- All requirements of a forward iterator
- Predecrementable
- Postdecrementable
- Supports expression: *a--

*/

// my iterator
template <typename T>
class MyIterator
{
public:
    // member types
    // using is synonym for the type
    // type of values obtained when dereferencing the iterator
    using value_type = T;
    // signed integer type to represent distance between iterators
    using difference_type = std::ptrdiff_t;
    // type of reference to type of values
    using reference = T &;
    //pointer to a variable of type T
    using pointer = T *;
    // category of the iterator
    using iterator_category = std::bidirectional_iterator_tag;

    // default ctor
    MyIterator() = default;

    // constructor
    MyIterator(T *cur, T *first_entry, T *last_entry)
        : cur(cur), first_entry(first_entry), last_entry(last_entry)
    {
    }
    // copy ctor
    MyIterator(MyIterator const &) = default;
    // copy assignment
    MyIterator &operator=(MyIterator const &) = default;
    // dtor
    ~MyIterator() = default;

    //& is because we pass the reference not pointer
    MyIterator<T> &operator++()
    {
        check();
        cur++;
        return *this;
    }

    MyIterator<T> operator++(int)
    {
        check();
        auto copy = *this;
        cur++;
        return copy;
    }

    MyIterator<T> &operator--()
    {
        check();
        cur--;
        return *this;
    }

    MyIterator<T> operator--(int)
    {
        check();
        auto copy = *this;
        cur--;
        return *this;
    }

    // to check if its pointing to the same one
    // const in front of MyIterator means the function cant change the iterator rhs
    // const in the end means it cant modify the iterator itself
    bool operator==(const MyIterator<T> &rhs) const
    {
        return cur == rhs.cur;
    }

    bool operator!=(const MyIterator<T> &rhs) const
    {
        return cur != rhs.cur;
    }

    // to dereference the iterator
    T &operator*()
    {
        check();
        return *cur;
    }

    T *operator->()
    {
        check();
        return cur;
    }

private:
    T *cur;
    T *first_entry;
    T *last_entry;

    void check()
    {
        if (cur < first_entry || cur > last_entry)
        {
            throw std::out_of_range("iterator is not valid");
        }
    }
};

#endif /* MY_ITERATOR_HPP */
