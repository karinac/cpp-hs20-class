# Programming Techniques for Scientific Simulations I
# HS 2020
# Exercise 7.1
# 2020-10-29
# Michal Sudwoj <msudwoj@student.ethz.ch>

# Require 3.1 for CMAKE_CXX_STANDARD property
cmake_minimum_required(VERSION 3.1)

# C++11
set(CMAKE_CXX_STANDARD 11)

# setting warning compiler flags
if(CMAKE_CXX_COMPILER_ID MATCHES "(C|c?)lang")
  add_compile_options(-Weverything)
else()
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

# add options
option(VERBOSE "Show what's happening" OFF)
if ( VERBOSE )
  add_definitions(-DVERBOSE)
endif()
option(MOVE "Enable move ctor & assignment" ON)
if ( MOVE )
  add_definitions(-DMOVE)
endif()

add_executable(main main.cpp)

