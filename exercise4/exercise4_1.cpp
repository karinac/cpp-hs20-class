

#include <iostream>

enum Z2
{
    Plus,
    Minus
};

template <class T>
T identity_element()
{
    return T(1);
}

template <>
Z2 identity_element()
{
    return Plus;
}

Z2 operator*(const Z2 lhs, const Z2 rhs)
{
    return (lhs == rhs) ? Plus : Minus;
}

template <class T>
T operator*(const Z2 lhs, const T rhs)
{
    return lhs == Plus ? rhs : -rhs; //if condition ? true : false
}

template <class T>
T operator*(const T lhs, const Z2 rhs)
{
    return rhs * lhs;
}

std::ostream &operator<<(std::ostream &lhs, const Z2 rhs)
{
    return lhs << (rhs == Plus ? '+' : '-');
}

template <class T>
T mypow(T a, const unsigned int n)
{
    T tmp = identity_element<T>();
    for (int i = 1; i <= n; i++)
    {
        tmp = tmp * a;
    }
    return tmp;
}

int main()
{
    // some testing: feel free to add your own!
    std::cout << Plus * Minus << std::endl;
    std::cout << Plus * -1 * Minus << std::endl;
    std::cout << (1. + 3.) * mypow(Minus, 4) << std::endl;
    for (unsigned i = 0; i < 7; i++)
        std::cout << "Plus^" << i << " = " << mypow(Plus, i) << std::endl;
    for (unsigned i = 0; i < 7; i++)
        std::cout << "Minus^" << i << " = " << mypow(Minus, i) << std::endl;
    for (unsigned i = 0; i < 7; i++)
        std::cout << "2^" << i << " = " << mypow(2.0, i) << std::endl;

    double a = 3.7;

    Z2 x = Minus;
    std::cout << x * a << std::endl;
    std::cout << a * x << std::endl;

    x = Plus;
    std::cout << x * a << std::endl;
    std::cout << a * x << std::endl;

    return 0;
}
