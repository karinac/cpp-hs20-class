the output would be :

a = green
b_ptr = orange
c = green 
d = orange
e = green
f = green 

deleting b_ptr would delete b_ptr but d stays since it just makes a copy of the object that b_ptr is pointing to