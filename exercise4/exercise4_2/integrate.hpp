#ifndef INTEGRATE_HPP
#define INTEGRATE_HPP

#include <functional>
#include <cassert>

//declares the simpson function
//inputs:
// f - function to integrate
// a - lower bound of the integration interval
// b - higher bound of the integration interval
// n - number of bins to use
//outputs :
// integral of the function over the interval
//preconditions :
// b > a && n > 0
template <class T>
float simpson(T a, T b, int n, std::function<T(T)> f)
{
    assert(n > 0); //conditions that must hold
    assert(b > a);
    assert(f != nullptr); //null pointer

    T dx = (b - a) / n;
    T sum = 0;
    for (int k = 0; k < n; ++k)
    {
        T x = a + k * dx;
        sum += f(x) + 4 * f(x + dx / 2) + f(x + dx);
    }
    return sum * dx / 6;
}

#endif