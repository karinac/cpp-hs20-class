#include <iostream>
#include <cmath>
#include "integrate.hpp"
#include <functional>

int main()
{
    float lambda = 2.0;
    //auto f = std::bind(g, std::placeholders::_1, lambda);

    std::cout << simpson<float>(0, 1, 128, [&](float x) {
        return exp(-lambda * x);
    }) << std::endl;

    return 0;
}
