#ifndef SIMPSON_INTEGRATION_HPP
#define SIMPSON_INTEGRATION_HPP

//declares the simpson function
//inputs: 
// f - function to integrate
// a - lower bound of the integration interval 
// b - higher bound of the integration interval
// n - number of bins to use
//outputs :
// integral of the function over the interval 
//preconditions :
// b > a && n > 0 

float my_sin(float x);
float Simpson_integration(int n, float a, float b, float (*f)(float));

#endif