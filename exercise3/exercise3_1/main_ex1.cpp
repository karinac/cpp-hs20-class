//this is the main function for question 3 that uses simpson integration function and investigate convergence

#include <iostream>
#include "Simpson_integration.hpp"
#include <fstream>

int main(){
    int N=5;
    double a=0;
    double b=3.14;
    double integral[100];

    //create a file for saving results
    std::ofstream myfile;
    myfile.open ("integrate.txt");
  
    //integrate over discritzation and save results in "integrate.txt"
    for (int i=0;i<100;i++){
        integral[i]=Simpson_integration(N, a, b, &my_sin);
        N=N+5;
        myfile << N << "    " << integral[i] << std::endl;
    }
       

}
