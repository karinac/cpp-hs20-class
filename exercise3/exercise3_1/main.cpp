#include <iostream>
#include <cmath>
#include "integrate.hpp"

float g(float x)
{
    return sin(x);
}


int main()
{
    for (int n = 2; n <= 15; ++n)
    {
            std::cout << n << " " << simpson(g, 0, M_PI, n) << std::endl ;
    }

    return 0;
}