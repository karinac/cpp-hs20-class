//simpson integration function 
#include <cassert>
#include <cmath>
#include "Simpson_integration.hpp"


float my_sin(float x)
{
    return sin(x);
}

float Simpson_integration(int n, float a, float b, float (*f)(float))
{
    assert(n > 0); //conditions that must hold 
    assert(b > a); 
    assert(f != nullptr); //null pointer 

    float dx = (b - a) / n;
    float sum = 0; 
    for (int k = 0; k < n; ++k)
    {
        float x = a + k * dx;
        sum += f(x) + 4 * f(x + dx / 2) + f(x + dx); 
    }
    return sum * dx / 6;
}

