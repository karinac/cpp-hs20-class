#include <iostream>
#include <limits>

long double machineeps(long double eps)
{
    long double temp; 

    while ( (1 + eps) != 1 )
    {
        temp = eps; 
        eps /= 2 ; 
    }

    return temp;
}

int main()
{
    long double precision = machineeps(0.5);
    std::cout << precision << std::endl;
    return 0;
}