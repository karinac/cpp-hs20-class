#include <iostream>
#include <vector>

int main() 
{
    //dynamic vector  
    std::vector<double> userinput ;
    int n;
    double sum = 0; 
   
    //read in n
    std::cin >> n;

    //read in n numbers 
    for (int k = 0; k < n; ++k)
    {
        double tmp;
        std::cin >> tmp; //can't directly push it into userinput k because no allocation yet 
        userinput.push_back(tmp);
        sum += tmp; 
    }

    // output normalize and reverse
    for (int k = n-1 ; k >= 0; --k)
    {
        std::cout << userinput[k] / sum << std::endl;
    }

    return 0;
}
