#include <iostream>

int main() {
  constexpr int n_max = 5; //at compiled time is contant

  //allocate memory for static array
  double userinput[n_max];
  int n;
  double sum = 0; 

  //read in n
  std::cin >> n;
  if (n > n_max)
  {
    std::cerr << "n is too large" << std::endl;
    return 1;
  }

  //read in n numbers 
  for (int k = 0; k < n; ++k)
  {
    std::cin >> userinput[k];
    sum += userinput[k];
   }

  //output normalize and reverse
  for (int k = n-1 ; k >= 0; --k)
  {
    std::cout << userinput[k] / sum << std::endl;
  }

  return 0;
}
