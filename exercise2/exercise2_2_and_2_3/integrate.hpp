#ifndef INTEGRATE_HPP
#define INTEGRATE_HPP

//declares the simpson function
//inputs: 
// f - function to integrate
// a - lower bound of the integration interval 
// b - higher bound of the integration interval
// n - number of bins to use
//outputs :
// integral of the function over the interval 
//preconditions :
// b > a && n > 0 

float simpson(float (*f)(float), float a, float b, int n);


#endif