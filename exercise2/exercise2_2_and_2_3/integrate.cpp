//simpson integration function 
#include <cassert>
#include "integrate.hpp"

float simpson(float (*f)(float), float a, float b, int n)
{
    assert(n > 0); //conditions that must hold 
    assert(b > a); 
    assert(f != nullptr); //null pointer 

    float dx = (b - a) / n;
    float sum = 0; 
    for (int k = 0; k < n; ++k)
    {
        float x = a + k * dx;
        sum += f(x) + 4 * f(x + dx / 2) + f(x + dx); 
    }
    return sum * dx / 6;
}

