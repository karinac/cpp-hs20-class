#include "simpson4.hpp"

#include <cassert>

namespace simpson4
{
    double integrate(const double a,
                     const double b,
                     const unsigned bins,
                     const Function &func)
    {
        assert(bins > 0);

        double dr = (b - a) / (2. * bins);
        double I2(0), I4(0);
        double pos = a;

        for (unsigned i = 0; i < bins; ++i)
        {
            pos += dr;
            I4 += func(pos);
            pos += dr;
            I2 += func(pos);
        }

        return (func(a) + 2. * I2 + 4. * I4 - func(b)) * (dr / 3.);
    }
} // namespace simpson4