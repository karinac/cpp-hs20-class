//simpson integration function
#include <cassert>
#include "simpson2.hpp"
#include <functional>

float simpson(float a, float b, int n, std::function<float(float)> f)
{
    assert(n > 0); //conditions that must hold
    assert(b > a);
    assert(f != nullptr); //null pointer

    float dx = (b - a) / n;
    float sum = 0;
    for (int k = 0; k < n; ++k)
    {
        float x = a + k * dx;
        sum += f(x) + 4 * f(x + dx / 2) + f(x + dx);
    }
    return sum * dx / 6;
}
