#ifndef SIMPSON_HPP
#define SIMPSON_HPP

#include <cmath>

namespace simpson4
{
  // abstract base class
  class Function
  {
  public:
    virtual double operator()(double) const = 0;
  };

  // functions for benchmarking
  class Function1 : public Function
  {
  public:
    double operator()(double x) const
    {
      return 0;
    }
  };

  class Function2 : public Function
  {
  public:
    double operator()(double x) const
    {
      return 1;
    }
  };

  class Function3 : public Function
  {
  public:
    double operator()(double x) const
    {
      return x;
    }
  };

  class Function4 : public Function
  {
  public:
    double operator()(double x) const
    {
      return x * x;
    }
  };

  class Function5 : public Function
  {
  public:
    double operator()(double x) const
    {
      return std::sin(x);
    }
  };

  class Function6 : public Function
  {
  public:
    double operator()(double x) const
    {
      return std::sin(5 * x);
    }
  };

  // simpson integration
  double integrate(double a, double b, unsigned bins, const Function &func);

} // namespace simpson4

#endif // SIMPSON_HPP
