#ifndef SIMPSON2_HPP
#define SIMPSON2_HPP

#include <functional>
#include <cassert>
#include <cmath>
//declares the simpson function
//inputs:
// f - function to integrate
// a - lower bound of the integration interval
// b - higher bound of the integration interval
// n - number of bins to use
//outputs :
// integral of the function over the interval
//preconditions :
// b > a && n > 0

namespace simpson2
{
    double f1(double x)
    {
        return 0;
    }

    double f2(double x)
    {
        return 1;
    }

    double f3(double x)
    {
        return x;
    }

    double f4(double x)
    {
        return x * x;
    }

    double f5(double x)
    {
        return std::sin(x);
    }

    double f6(double x)
    {
        return std::sin(5 * x);
    }

    template <class T>
    double integrate(T a, T b, unsigned n, T (*f)(T))
    {
        assert(n > 0); //conditions that must hold
        assert(b > a);
        assert(f != nullptr); //null pointer

        T dx = (b - a) / n;
        T sum = 0;
        for (unsigned k = 0; k < n; ++k)
        {
            T x = a + k * dx;
            sum += f(x) + 4 * f(x + dx / 2) + f(x + dx);
        }
        return sum * dx / 6;
    }
} // namespace simpson2

#endif