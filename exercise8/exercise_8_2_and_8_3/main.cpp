#include <cmath>
#include <iostream>
#include <functional>
#include "simpson2.hpp"
#include "simpson3.hpp"
#include "simpson4.hpp"
#include "timer.hpp"

const unsigned n = 1e6;
const unsigned bins = 200;

template <typename T>
void benchmark2(const T &f)
{
    Timer t;
    double result;

    t.start();
    for (unsigned i = 0; i < n; ++i)
    {
        result = simpson2::integrate(0.0, 1.0, bins, f);
    }
    t.stop();

    std::cout << "variant 2: result " << result << std::endl;
    std::cout << "variant 2: duration " << t.duration() / n * 1e9 << " ns" << std::endl;
}
template <typename T>
void benchmark3(const T &f)
{
    Timer t;
    double result;

    t.start();
    for (unsigned i = 0; i < n; ++i)
    {
        result = simpson3::integrate(0, 1, bins, f);
    }
    t.stop();

    std::cout << "variant 3: result " << result << std::endl;
    std::cout << "variant 3: duration " << t.duration() / n * 1e9 << " ns" << std::endl;
}

template <typename T>
void benchmark4(const T &f)
{
    Timer t;
    double result;

    t.start();
    for (unsigned i = 0; i < n; ++i)
    {
        result = simpson4::integrate(0, 1, bins, f);
    }
    t.stop();

    std::cout << "variant 4: result " << result << std::endl;
    std::cout << "variant 4: duration " << t.duration() / n * 1e9 << " ns" << std::endl;
}

int main()
{
    std::cout << "Function 1" << std::endl;
    benchmark2(simpson2::f1);
    benchmark3(simpson3::f1);
    benchmark4(simpson4::Function1());
    std::cout << std::endl;

    std::cout << "Function 2" << std::endl;
    benchmark2(simpson2::f2);
    benchmark3(simpson3::f2);
    benchmark4(simpson4::Function2());
    std::cout << std::endl;

    std::cout << "Function 3" << std::endl;
    benchmark2(simpson2::f3);
    benchmark3(simpson3::f3);
    benchmark4(simpson4::Function3());
    std::cout << std::endl;

    std::cout << "Function 4" << std::endl;
    benchmark2(simpson2::f4);
    benchmark3(simpson3::f4);
    benchmark4(simpson4::Function4());
    std::cout << std::endl;

    std::cout << "Function 5" << std::endl;
    benchmark2(simpson2::f5);
    benchmark3(simpson3::f5);
    benchmark4(simpson4::Function5());
    std::cout << std::endl;

    std::cout << "Function 6" << std::endl;
    benchmark2(simpson2::f6);
    benchmark3(simpson3::f6);
    benchmark4(simpson4::Function6());

    return 0;
}