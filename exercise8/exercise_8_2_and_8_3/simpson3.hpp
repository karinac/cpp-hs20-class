#ifndef SIMPSON3_HPP
#define SIMPSON3_HPP

#include <cassert>
#include <cmath>

namespace simpson3
{
  // benchmark functions
  double f1(double x) { return 0; }
  double f2(double x) { return 1; }
  double f3(double x) { return x; }
  double f4(double x) { return x * x; }
  double f5(double x) { return std::sin(x); }
  double f6(double x) { return std::sin(5 * x); }

  template <typename F>
  struct domain_t
  {
    using type = typename F::input_t;
  };

  template <typename T, typename U>
  struct domain_t<T(U)>
  {
    using type = U;
  };

  template <typename F>
  struct result_t
  {
    using type = typename F::output_t;
  };

  template <typename T, typename U>
  struct result_t<T(U)>
  {
    using type = T;
  };

  template <typename F>
  typename result_t<F>::type integrate(const typename domain_t<F>::type a,
                                       const typename domain_t<F>::type b,
                                       const unsigned bins,
                                       const F &func)
  {
    assert(bins > 0);

    const typename domain_t<F>::type dr = (b - a) / (2. * bins);
    typename result_t<F>::type I2(0), I4(0);
    typename domain_t<F>::type pos = a;

    for (unsigned int i = 0; i < bins; ++i)
    {
      pos += dr;
      I4 += func(pos);
      pos += dr;
      I2 += func(pos);
    }

    return (func(a) + 2. * I2 + 4. * I4 - func(b)) * (dr / 3.);
  }
} // namespace simpson3

#endif // SIMPSON3_HPP
