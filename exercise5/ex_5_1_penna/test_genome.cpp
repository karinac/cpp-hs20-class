#include <cstdlib>
#include <ctime>
#include <iostream>
#include "genome.hpp"

int main()
{
    // Seed random number generator
    std::srand(std::time(0));

    Penna::Genome::set_mutation_rate(5);
    Penna::Genome g;

    // Mutate 10 times and output the number of bad genes
    for (Penna::age_t i = 1; i <= 10; ++i)
    {
        g.mutate();
        std::cout << g.count_bad(i) << std::endl;
    }

    return 0;
}