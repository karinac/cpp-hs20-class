#include <cstdlib>

#include "animal.hpp"

Penna::age_t Penna::Animal::bad_threshold_ = 1;
Penna::age_t Penna::Animal::maturity_age_ = 1;
double Penna::Animal::probability_to_get_pregnant_ = 1.0;

void Penna::Animal::set_maturity_age(age_t R)
{
    maturity_age_ = R;
}

void Penna::Animal::set_probability_to_get_pregnant(double B)
{
    probability_to_get_pregnant_ = B;
}

void Penna::Animal::set_bad_threshold(age_t T)
{
    bad_threshold_ = T;
}

Penna::age_t Penna::Animal::get_bad_threshold()
{
    return bad_threshold_;
}

Penna::Animal::Animal()
    : genome_(), age_(0), is_pregnant_(false)
{
}

Penna::Animal::Animal(const Genome &g)
    : genome_(g), age_(0), is_pregnant_(false)
{
}

bool Penna::Animal::is_dead() const
{
    return genome_.count_bad(age_) > bad_threshold_;
}

bool Penna::Animal::is_pregnant() const
{
    return is_pregnant_;
}

Penna::age_t Penna::Animal::age() const
{
    return age_;
}

void Penna::Animal::grow()
{
    age_++;

    if (age_ <= maturity_age_)
    {
        return;
    }

    if (std::rand() / ((double)RAND_MAX + 1) < probability_to_get_pregnant_)
    {
        is_pregnant_ = true;
    }
}

Penna::Animal Penna::Animal::give_birth()
{
    is_pregnant_ = false;
    Genome g(genome_);
    g.mutate();
    return Animal(g);
}
