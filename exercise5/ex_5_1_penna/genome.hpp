/**
 * Header for Penna genome class.
 * Programming Techniques for Scientific Simulations, HS 2020
 */

#ifndef GENOME_HPP
#define GENOME_HPP

#include <bitset>
#include <limits>

namespace Penna
{
    // Type for age of Genome holders
    using age_t = unsigned int;

    // Genome class representing each gene with one bit
    class Genome
    {
    public:
        static const age_t number_of_genes =
            std::numeric_limits<unsigned long>::digits;

        // Set rate of mutation (M in Penna's paper)
        static void set_mutation_rate(age_t);

        // Default constructor: initialize genes to all good
        Genome() = default;

        // Count number of bad genes in first n years
        age_t count_bad(age_t n) const;

        // Mutate the genome by flipping M genes
        void mutate();

    private:
        static age_t mutation_rate_;
        std::bitset<number_of_genes> genes_;
    };
} // namespace Penna

#endif // GENOME_HPP
