#include <cstdlib>
#include <iostream>
#include <list>

#include "animal.hpp"

int main()
{
    // example from Penna's paper
    std::size_t N_max = 1e5; // type which can hold sizes
    Penna::Animal::set_maturity_age(8);
    Penna::Animal::set_probability_to_get_pregnant(1.0);
    Penna::Animal::set_bad_threshold(2);
    Penna::Genome::set_mutation_rate(2);
    std::size_t num_steps = 1000;

    std::list<Penna::Animal> population(N_max);

    for (std::size_t t = 1; t <= num_steps; ++t)
    {
        std::size_t pop_size = population.size();

        for (auto it = population.begin(); it != population.end();)
        {
            it->grow();

            // remove the animal if it is dead or it starved
            double random = std::rand() / ((double)RAND_MAX + 1);
            double starve_p = population.size() / (double)(N_max);
            if (it->is_dead() || random < starve_p)
            {
                it = population.erase(it);
                continue;
            }

            if (it->is_pregnant())
            {
                population.push_front(it->give_birth());
            }
            ++it;
        }
        std::cout << "population size = " << population.size() << std::endl;
    }
    return 0;
}