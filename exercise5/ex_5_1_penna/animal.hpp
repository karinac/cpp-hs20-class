/**
 * Header for Penna animal class.
 * Programming Techniques for Scientific Simulations, HS 2020
 */

#ifndef ANIMAL_HPP
#define ANIMAL_HPP

#include "genome.hpp"

namespace Penna
{
    // Animal with a Genome and age
    class Animal
    {
    public:
        // Upper age limit
        static const age_t maximum_age = Genome::number_of_genes;

        // Set maturity age
        static void set_maturity_age(age_t);

        // Set birth rate modifier
        static void set_probability_to_get_pregnant(double);

        // Set threshold for deletrious mutation
        static void set_bad_threshold(age_t);

        // Get deletrious mutation threshold
        static age_t get_bad_threshold();

        // Constructor: uses all good genome
        Animal();

        // Construct Animal from Genome
        Animal(const Genome &);

        bool is_dead() const;
        bool is_pregnant() const;
        age_t age() const;

        // Grow the animal older by one year
        void grow();

        // Create baby with inherited, randomly mutated genes
        Animal give_birth();

    private:
        // Number T of bad genes an animal can tolerate
        static age_t bad_threshold_;

        // Maturity age parameter R
        static age_t maturity_age_;

        // Birth rate modifier B
        static double probability_to_get_pregnant_;

        const Genome genome_;
        age_t age_;
        bool is_pregnant_;
    };
} // namespace Penna

#endif // ANIMAL_HPP
