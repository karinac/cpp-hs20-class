#include <cstdlib>

#include "genome.hpp"

Penna::age_t Penna::Genome::mutation_rate_ = 1;

void Penna::Genome::set_mutation_rate(age_t new_mutation_rate)
{
    mutation_rate_ = new_mutation_rate;
}

Penna::age_t Penna::Genome::count_bad(age_t n) const
{
    age_t count = 0;
    for (age_t i = 0; i < n; ++i)
    {
        if (genes_[i])
        {
            count++;
        }
    }
    return count;
}

void Penna::Genome::mutate()
{
    for (age_t i = 0; i < mutation_rate_; ++i)
    {
        age_t pos = std::rand() % number_of_genes;
        genes_.flip(pos);
    }
}
