/* Programming Techniques for Scientific Simulations, HS 2020
 * Exercise 4.2
 */

#ifndef SIMPSON_HPP
#define SIMPSON_HPP

#include <cassert>

// domain_t type traits
template <typename F>
struct domain_t
{
  using type = typename F::input_t;
};

template <typename T, typename U>
struct domain_t<T(U)>
{
  using type = U;
};

// result_t type traits
template <typename F>
struct result_t
{
  using type = typename F::output_t;
};

template <typename T, typename U>
struct result_t<T(U)>
{
  using type = T;
};

// Concepts needed for type F:
//     - F needs to be a function or function object taking a single argument convertible from T,
//       with return value convertible to T.
// Concepts needed for type T:
//     - CopyConstructible
//     - Assignable
//     - T shall support arithmetic operations with double with result convertible to T with
//       limited relative truncation errors.
template <typename F>
typename result_t<F>::type integrate(const typename domain_t<F>::type a,
                                     const typename domain_t<F>::type b,
                                     const unsigned bins,
                                     const F &func)
{
  assert(bins > 0);

  const typename domain_t<F>::type dr = (b - a) / (2. * bins);
  typename result_t<F>::type I2(0), I4(0);
  typename domain_t<F>::type pos = a;

  for (unsigned int i = 0; i < bins; ++i)
  {
    pos += dr;
    I4 += func(pos);
    pos += dr;
    I2 += func(pos);
  }

  return (func(a) + 2. * I2 + 4. * I4 - func(b)) * (dr / 3.);
}

#endif // SIMPSON_HPP
