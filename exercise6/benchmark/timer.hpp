#ifndef TIMER_HPP
#define TIMER_HPP

#include <chrono>

class Timer
{
public:
    void start();
    void stop();
    std::chrono::duration<double> duration() const;

private:
    std::chrono::time_point<std::chrono::steady_clock> starttime;
    std::chrono::time_point<std::chrono::steady_clock> endtime;
};

#endif
