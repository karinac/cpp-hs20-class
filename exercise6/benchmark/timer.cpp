#include <chrono>
#include "timer.hpp"

void Timer::start()
{
    starttime = std::chrono::steady_clock::now();
}

void Timer::stop()
{
    endtime = std::chrono::steady_clock::now();
}

std::chrono::duration<double> Timer::duration() const
{
    return starttime - endtime;
}