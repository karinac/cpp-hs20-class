#ifndef IS_ALLOWED_HPP
#define IS_ALLOWED_HPP
#include <type_traits>

template <typename T>
using is_allowed = std::is_integral<T>;

#endif