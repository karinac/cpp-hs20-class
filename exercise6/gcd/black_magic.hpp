#ifndef BLACK_MAGIC_HPP
#define BLACK_MAGIC_HPP

template <bool B, class T = void>
struct wish_come_true
{
};

template <typename T>
struct wish_come_true<true, T>
{
    using type = T;
};

#endif
