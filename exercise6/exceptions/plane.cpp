#include <iostream>
#include <stdexcept>

class Plane
{
public:
    void start()
    {
        std::cout << "Plane started successfully." << std::endl;
    }

    void serve_food()
    {
        throw std::runtime_error("The food is not edible!");
    }

    void land()
    {
        std::cout << "Plane landed successfully." << std::endl;
    }

    void make_emergency_landing(std::exception &e)
    {
        std::cout << "Plane made an emergency landing because:" << std::endl;
        std::cout << e.what() << std::endl;
    }
};

int main()
{
    Plane plane;
    plane.start();
    try
    {
        plane.serve_food();
        plane.land();
    }
    catch (std::exception &e)
    {
        plane.make_emergency_landing(e);
    }
}
